<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Jet test

После скачивания проекта:

- cp .env.example .env
- Указать свои данные бд в env
- composer install
- php artisan optimize
- php artisan l5-swagger:generate

https://gitlab.com/Sarsebek/jet_test
