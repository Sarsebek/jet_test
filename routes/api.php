<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;
use App\Http\Controllers\GenreController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'game'], function ($route) {

    $route->get('/', [GameController::class, 'index']);
    $route->post('/', [GameController::class, 'store']);
    $route->post('/{id}', [GameController::class, 'update']);
    $route->delete('/{id}', [GameController::class, 'destroy']);

});

Route::group(['prefix' => 'genre'], function ($route) {

    $route->get('/', [GenreController::class, 'index']);
    $route->post('/', [GenreController::class, 'store']);
    $route->post('/{id}', [GenreController::class, 'update']);
    $route->delete('/{id}', [GenreController::class, 'destroy']);

});
