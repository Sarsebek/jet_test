<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * @OA\Get(
     * path="/api/game",
     * summary="Получение игр",
     * operationId="game.index",
     * tags={"Игры"},
     * @OA\Response(
     *    response=500,
     *    description="Что-то пошло не так",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Что-то пошло не так")
     *        )
     *     ),
     *  @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="data")
     *        )
     *  )
     * )
     */
    public function index()
    {
        $games = Game::all()->load('genres');

        return response()->json(['data' => $games]);
    }

    /**
     * @OA\Post(
     * path="/api/game",
     * summary="Создание игры",
     * description="Создание игры",
     * operationId="game.store",
     * tags={"Игры"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Данные игры",
     *    @OA\JsonContent(
     *       required={"name", "description", "genres"},
     *       @OA\Property(property="name", type="string", example="Название"),
     *       @OA\Property(property="description", type="string", example="Описание"),
     *       @OA\Property(property="genres", type="array", @OA\Items(
     *        type="integer",
     *     ))
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешно создали",
     *     @OA\JsonContent(
     *       @OA\Property(property="data", type="object", ref="#/components/schemas/Game")
     *        )
     *    ),
     * @OA\Response(
     *    response=500,
     *    description="Ошибка",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Что-то пошло не так, попробуйте позднее!")
     *        )
     *     )
     *  )
     */
    public function store(Request $request)
    {
        $game = Game::query()->create($request->only(['name', 'description']));

        // жанры
        $game->genres()->sync($request->input('genres'), []);

        return response()->json(['data' => $game->load('genres')]);
    }

    /**
     * @OA\Post(
     * path="/api/game/{game_id}",
     * summary="Редактирование игры",
     * description="Редактирование игры",
     * operationId="game.update",
     * tags={"Игры"},
     * @OA\Parameter(
     *    description="Game Id",
     *    in="path",
     *    name="game_id",
     *    required=true,
     *    example="1",
     *    @OA\Schema(
     *       type="integer",
     *       format="int64"
     *    )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Данные игры",
     *    @OA\JsonContent(
     *       required={"name", "description", "genres"},
     *       @OA\Property(property="name", type="string", example="Название"),
     *       @OA\Property(property="description", type="string", example="Описание"),
     *       @OA\Property(property="genres", type="array", @OA\Items(
     *        type="integer",
     *     ))
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешно обновили",
     *     @OA\JsonContent(
     *       @OA\Property(property="data", type="object", ref="#/components/schemas/Game")
     *        )
     *    ),
     * @OA\Response(
     *    response=422,
     *    description="Неправильный id",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Неправильный id")
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Ошибка",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Что-то пошло не так, попробуйте позднее!")
     *        )
     *     )
     *  )
     */

    public function update($id, Request $request)
    {
        $game = Game::query()->where('id', $id)->first();

        if (!$game)
            return response()->json(['error' => 'Неправильный id'], 422);

        $game->update($request->only(['name', 'description']));

        $game->save();

        // жанры
        $game->genres()->detach();
        $game->genres()->sync($request->input('genres'), []);

        return response()->json(['data' => $game->load('genres')]);
    }

    /**
     * @OA\Delete(
     * path="/api/game/{game_id}",
     * summary="Удаление игры",
     * description="Удаление игры",
     * operationId="game.delete",
     * tags={"Игры"},
     *   @OA\Parameter(
     *    description="ID игры",
     *    in="path",
     *    name="game_id",
     *    required=true,
     *    example="1",
     *    @OA\Schema(
     *       type="integer",
     *       format="int64"
     *    )
     *    ),
     * @OA\Response(
     *    response=200,
     *    description="Успешно удалили",
     *     @OA\JsonContent(
     *       @OA\Property(property="success", type="string", example="Успешно удалили")
     *        )
     *    ),
     * @OA\Response(
     *    response=422,
     *    description="Неправильный айди",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Неправильный id")
     *        )
     *     ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Ошибка",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Что-то пошло не так, попробуйте позднее!")
     *        )
     *     )
     *  )
     */
    public function destroy($id)
    {
        $game = Game::query()->where('id', $id)->first();

        if (!$game)
            return response()->json(['error' => 'Неправильный id'], 422);

        $game->delete();

        return response()->json(['success' => 'Успешно удалили']);
    }
}
