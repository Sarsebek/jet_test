<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * @OA\Get(
     * path="/api/genre",
     * summary="Получение жанров",
     * operationId="genre.index",
     * tags={"Жанры"},
     * @OA\Response(
     *    response=500,
     *    description="Что-то пошло не так",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Что-то пошло не так")
     *        )
     *     ),
     *  @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *       @OA\Property(property="data")
     *        )
     *  )
     * )
     */
    public function index()
    {
        $genres = Genre::all();

        return response()->json(['data' => $genres]);
    }

    /**
     * @OA\Post(
     * path="/api/genre",
     * summary="Создание жанра",
     * description="Создание жанра",
     * operationId="Genre.store",
     * tags={"Жанры"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Данные жанра",
     *    @OA\JsonContent(
     *       required={"name", "description"},
     *       @OA\Property(property="name", type="string", example="Название"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешно создали",
     *     @OA\JsonContent(
     *       @OA\Property(property="data", type="object", ref="#/components/schemas/Genre")
     *        )
     *    ),
     * @OA\Response(
     *    response=500,
     *    description="Ошибка",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Что-то пошло не так, попробуйте позднее!")
     *        )
     *     )
     *  )
     */
    public function store(Request $request)
    {
        $genre = Genre::query()->create($request->only(['name']));

        // жанры

        return response()->json(['data' => $genre]);
    }

    /**
     * @OA\Post(
     * path="/api/genre/{genre_id}",
     * summary="Редактирование жанра",
     * description="Редактирование жанра",
     * operationId="Genre.update",
     * tags={"Жанры"},
     * @OA\Parameter(
     *    description="genre Id",
     *    in="path",
     *    name="genre_id",
     *    required=true,
     *    example="1",
     *    @OA\Schema(
     *       type="integer",
     *       format="int64"
     *    )
     * ),
     * @OA\RequestBody(
     *    required=true,
     *    description="Данные жанра",
     *    @OA\JsonContent(
     *       required={"name", "description"},
     *       @OA\Property(property="name", type="string", example="Название"),
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Успешно обновили",
     *     @OA\JsonContent(
     *       @OA\Property(property="data", type="object", ref="#/components/schemas/Genre")
     *        )
     *    ),
     * @OA\Response(
     *    response=422,
     *    description="Неправильный id",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Неправильный id")
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Ошибка",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Что-то пошло не так, попробуйте позднее!")
     *        )
     *     )
     *  )
     */

    public function update($id, Request $request)
    {
        $genre = Genre::query()->where('id', $id)->first();

        if (!$genre)
            return response()->json(['error' => 'Неправильный id'], 422);

        $genre->update($request->only(['name']));

        $genre->save();

        // жанры

        return response()->json(['data' => $genre]);
    }

    /**
     * @OA\Delete(
     * path="/api/genre/{genre_id}",
     * summary="Удаление жанра",
     * description="Удаление жанра",
     * operationId="Genre.delete",
     * tags={"Жанры"},
     *   @OA\Parameter(
     *    description="ID жанра",
     *    in="path",
     *    name="genre_id",
     *    required=true,
     *    example="1",
     *    @OA\Schema(
     *       type="integer",
     *       format="int64"
     *    )
     *    ),
     * @OA\Response(
     *    response=200,
     *    description="Успешно удалили",
     *     @OA\JsonContent(
     *       @OA\Property(property="success", type="string", example="Успешно удалили")
     *        )
     *    ),
     * @OA\Response(
     *    response=422,
     *    description="Неправильный айди",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Неправильный id")
     *        )
     *     ),
     *
     * @OA\Response(
     *    response=500,
     *    description="Ошибка",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Что-то пошло не так, попробуйте позднее!")
     *        )
     *     )
     *  )
     */
    public function destroy($id)
    {
        $genre = Genre::query()->where('id', $id)->first();

        if (!$genre)
            return response()->json(['error' => 'Неправильный id'], 422);

        $genre->delete();

        return response()->json(['success' => 'Успешно удалили']);
    }
}
