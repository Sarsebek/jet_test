<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * @OA\Property(property="name", type="string"),
 * )
 * Class Genre
 *
 */
class Genre extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    protected $casts = [
        'updated_at' => 'timestamp',
        'created_at' => 'timestamp',
    ];

}
