<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * @OA\Property(property="name", type="string"),
 * @OA\Property(property="description", type="string"),
 * )
 * Class Game
 *
 */
class Game extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description'];

    protected $casts = [
        'updated_at' => 'timestamp',
        'created_at' => 'timestamp',
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }
}
